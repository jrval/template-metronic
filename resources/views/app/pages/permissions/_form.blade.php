<form id="form" class="kt-form">
    @csrf
    @isset($permission)
        {{ method_field('PUT') }}
    @endisset
    <div class="form-group">
        <label>Permission Name <span class="required">*</span>
        </label>
            <input type="text" id="name" required="required"  name="name" value="{{$permission->name ?? ''}}" class="form-control">
    </div>
</form>
