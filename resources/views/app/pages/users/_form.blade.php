<form id="form" class="kt-form">
    @csrf
    @isset($user)
        {{ method_field('PUT') }}
    @endisset
    <div class="form-group">
        <label>Full Name <span class="required">*</span>
        </label>
        <input type="text" name="name" required="required" value="{{$user->name ?? ''}}" class="form-control" placeholder="Enter full name">
    </div>
    <div class="form-group">
        <label>Username <span class="required">*</span>
        </label>
        <input placeholder="Enter username" type="text" name="username" required="required" value="{{$user->username ?? ''}}" class="form-control">
    </div>
{{--    <div class="form-group">--}}
{{--        <label>Email <span class="required">*</span>--}}
{{--        </label>--}}
{{--        <input type="text" placeholder="Enter email" name="email" required="required" value="{{$user->email ?? ''}}" class="form-control">--}}
{{--    </div>--}}
    <div class="form-group">
        <label>Password <span class="required">*</span>
        </label>
            <input type="password" placeholder="Enter password" name="password" required="required" value="" class="form-control">
            @isset($user)
                <small class="text-navy">Leave blank if you don't want to change password</small>
            @endif
    </div>
    <div class="form-group">
        <label>Confirm Password <span class="required">*</span>
        </label>
            <input placeholder="Re enter password" type="password" name="password_confirmation" required="required" value="" class="form-control">

    </div>
    <div class="form-group">
        <label>Select Role <span class="required">*</span>
            <br>
            <small class="text-navy">User can have 1 or more roles</small>
        </label>
        <div class="kt-checkbox-inline">
            @forelse($roles as $key=>$role)
                @if(isset($user))
                    <label class="kt-checkbox">
                        <input type="checkbox" name="roles[]" value="{{$role->name}}"
                           @foreach($user->roles()->pluck('name') as $user_role)
                               @if($user_role== $role->name)
                                   checked="checked"
                                @endif
                            @endforeach
                        > {{$role->name}}
                        <span></span>
                    </label>
                @else
                    <label class="kt-checkbox">
                        <input type="checkbox" name="roles[]" value="{{$role->name}}"
                        > {{$role->name}}
                        <span></span>
                    </label>
                @endif
            @empty
                <div class="checkbox">
                    <label>
                        No Roles
                    </label>
                </div>
            @endforelse
        </div>
    </div>
</form>
