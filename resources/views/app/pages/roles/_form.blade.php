<form id="form" class="kt-form">
    @csrf
    @isset($role)
        {{ method_field('PUT') }}
    @endisset
    <div class="form-group">
        <label>Role Name <span class="required">*</span>
        </label>
            <input type="text" name="name" required="required" value="{{$role->name ?? ''}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Select Permission <span class="required">*</span>
            <br>
            <small class="text-navy">Role can have 1 or more permissions</small>
        </label>
        <div class="kt-checkbox-inline">
            @forelse($permissions as $key=>$permission)
                @if(isset($role))
                    <label class="kt-checkbox">
                        <input type="checkbox" name="permissions[]" value="{{$permission->name}}"
                               @foreach($role->permissions()->pluck('name') as $roles)
                               @if($roles== $permission->name)
                               checked="checked"
                            @endif
                            @endforeach
                        > {{$permission->name}}
                        <span></span>
                    </label>
                @else
                    <label class="kt-checkbox">
                        <input type="checkbox"name="permissions[]" value="{{$permission->name}}"
                        > {{$permission->name}}
                        <span></span>
                    </label>
                @endif
            @empty
                <div class="checkbox">
                    <label>
                        No Permission
                    </label>
                </div>
            @endforelse
        </div>
    </div>
</form>
