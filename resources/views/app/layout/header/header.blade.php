<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->
   @include('app.layout.header._header-menu')

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    @include('app.layout.header._header-topbar')
    <!-- end:: Header Topbar -->
</div>

<!-- end:: Header -->
