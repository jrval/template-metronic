<div class="modal fade" id="modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
            </div>
            <div class="alert alert-danger  error-modal" role="alert" style="display: none;">
            </div>
            <div class="alert alert-success  success-modal" role="alert" style="display: none;">
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-modal-submit"></button>
            </div>
        </div>
    </div>
</div>

