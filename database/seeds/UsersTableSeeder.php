<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'permission_create']);
        Permission::create(['name' => 'permission_delete']);
        Permission::create(['name' => 'permission_update']);
        Permission::create(['name' => 'permission_view']);
        Permission::create(['name' => 'role_create']);
        Permission::create(['name' => 'role_delete']);
        Permission::create(['name' => 'role_update']);
        Permission::create(['name' => 'role_view']);
        Permission::create(['name' => 'user_create']);
        Permission::create(['name' => 'user_disable']);
        Permission::create(['name' => 'user_enable']);
        Permission::create(['name' => 'user_update']);
        Permission::create(['name' => 'user_view']);

        $role = Role::create(['name' => 'super_administrator']);
        $role->givePermissionTo(['permission_create','permission_delete','permission_update','permission_view',
            'role_create','role_delete','role_update','role_view','user_create','user_disable','user_enable','user_update',
            'user_view']);

        //Create Super admin
        $user = User::create([
            'name' => 'Super Administrator',
            'username' => 'sa',
            'email'=>'sa@test.com',
            'password' => 'P4ssw0rd',
        ]);

        $user->assignRole('super_administrator');
    }
}
