<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Yajra\DataTables\Facades\DataTables;

class User extends Authenticatable
{
    use  HasRoles,Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at','updated_at'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:M d, Y h:i:s a',
    ];

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function scopeHasRoles(\Illuminate\Database\Eloquent\Builder $query, $roles)
    {
        return $this->scopeRole($query, $roles);
    }

    public static function usersDatatable(){

        $data = User::query()->withTrashed()->with('roles')
            ->select('users.*');
        return DataTables::eloquent($data)
            ->addColumn('action', function ($data) {
                    $html='';
                if($data->deleted_at ==  null){
                    $html='
                          <a href="'.route('users.show',$data->id).'" class="btn btn-sm btn-success btn-icon btn-icon-md" title="View">
                          <i class="la la-eye"></i>
                        </a>
                        ';
                    $html.='
                         <a href="" class="btn btn-sm btn-warning btn-icon btn-icon-md" data-id="'.$data->id.'" 
                         data-toggle="modal" data-target="#modal-lg" title="Edit" data-btn="edit">
                          <i class="la la-edit"></i>
                        </a>
                        ';
                    $html.='
                        <a href="" class="btn btn-sm btn-danger btn-icon btn-icon-md" data-btn="delete" data-id="'.$data->id.'" 
                        data-toggle="modal" data-target="#modal-lg-delete" title="Disable">
                          <i class="la la-ban"></i>
                        </a>
                        
                        ';
                }else{
                    $html.='
                        <a href="" class="btn btn-sm btn-info btn-icon btn-icon-md" data-btn="enable" data-id="'.$data->id.'" 
                        data-toggle="modal" data-target="#modal-lg-delete" title="Enable">
                          <i class="la la-check-circle"></i>
                        </a>
                    
                        ';
                }

                return  $html;
            })
            ->addColumn('roles', function ($data) {
                $html = '';
                if(isset($data->roles)){
                    foreach($data->roles()->pluck('name') as $role){
                        $html.= ' <span class="badge badge-primary">'.$role.'</span>';
                    }
                }
                return  $html;
            })
            ->filterColumn('roles', function ($query, $keyword) {
                $query ->selectraw("select name from roles where name = ?", ["%$keyword%"]);
            })
            ->rawColumns(['roles','action'])
            ->make(true);
    }

    public static function userStore($request){

        //create user
        $user = User::create($request->except('roles','password_confirmation'));
        //assigning roles to user
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        if($user){
            return  response()->json([
                'status'=>'success',
                'message'=>'User successfully created'
            ]);
        }
    }
}
