<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if (! Gate::allows('role_view')) {
//            return abort(403);
//        }
        return view('app.pages.roles.index');
    }

    public function roleDatatable(){
        if (! Gate::allows('role_view')) {
            return abort(403);
        }
        $data = \App\Role::query();
        return DataTables::of($data)
            ->addColumn('permissions',function ($data){
                $permissions = $data->permissions()->pluck('name');
                $html='';
                if(count($permissions)>0){
                    foreach($permissions as $permission) {
                        $html .= ' <span class="badge badge-primary">' . $permission . '</span>';
                    }
                }else{
                    $html.='<p>No Permission</p>';
                }

                return $html;
            })
            ->addColumn('action', function ($data) {
                $html='<a class="btn btn-sm btn-warning btn-icon btn-icon-md" title="Edit" href="" type="button" data-btn="edit" data-id="'.$data->id.'" data-toggle="modal" data-target="#modal-lg">
                       <i class="la la-edit"></i>
                        </a>';
                $html.='<a href="" class="btn btn-sm btn-danger btn-icon btn-icon-md" title="Delete"  type="button" data-btn="delete" data-id="'.$data->id.'" data-toggle="modal" data-target="#modal-lg-delete">
                        <i class="la la-trash"></i>
                        </a>';
                return  $html;
            })
            ->rawColumns(['action','permissions'])
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('role_create')) {
            return abort(403);
        }
        $permissions = Permission::all();
        //render form
        $form = view()->make('app.pages.roles._form',compact('permissions'))->render();
        //save to variable
        $data = array(
            'form' => $form,
        );
        //return json
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('role_create')) {
            return abort(403);
        }

        $request->validate([
            'name' => 'required|unique:roles,name|regex:/(^[A-Za-z0-9-_]+$)+/',
            'permissions'=>'required'
        ]);
        $role = Role::create($request->except('permissions'));
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        $role->givePermissionTo($permissions);
        if($role){
            return  response()->json([
                'status'=>'success',
                'message'=>'Role Successfully Added'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('role_update')) {
            return abort(403);
        }
        $permissions = Permission::all();
        $role = Role::findOrFail($id);
        $form = view()->make('app.pages.roles._form',compact('permissions','role'))->render();
        //save to variable
        $data = array(
            'form' => $form,
        );
        //return json
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('role_update')) {
            return abort(403);
        }
        $request->validate([
            'name' => 'required|unique:roles,name,'.$request->role.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'permissions'=>'required'
        ]);

        $role = Role::findOrFail($id);
        $role->update($request->except('permissions'));
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        $role->syncPermissions($permissions);

        if($role){
            return  response()->json([
                'status'=>'success',
                'message'=>'Role Successfully Updated'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('role_delete')) {
            return abort(403);
        }
        $role = Role::findOrFail($id);
        $role->delete();

        if($role){
            return  response()->json([
                'status'=>'success',
                'message'=>'Role Successfully Deleted',
            ]);
        }
    }
}
