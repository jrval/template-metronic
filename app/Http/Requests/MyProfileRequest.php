<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MyProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd(auth()->user());
        $rules = [
            'name' => 'required',
            'username' => 'sometimes|max:255|unique:users,username,'.auth()->user()->id.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'password' => 'nullable',
            'password_confirmation' => 'required_with:password|same:password',
        ];

        return $rules;
    }
}
