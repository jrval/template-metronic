<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//
Auth::routes();
//Auth::loginUsingId(1);
Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', function(){
        return redirect('home');
    })->name('home');
    /*permissions*/
    Route::resource('permissions', 'PermissionController');
    Route::get('permissions-datatable', 'PermissionController@permissionDatatable')->name('permissions.datatable');

    /*roles*/
    Route::resource('roles', 'RoleController');
    Route::get('roles-datatable', 'RoleController@roleDatatable')->name('roles.datatable');

    /*users*/
    Route::resource('users', 'UserController');
    Route::DELETE('users-enabled/{user}', 'UserController@userEnabled')->name('users.enabled');
    Route::get('users-datatable', 'UserController@usersDatatable')->name('users.datatable');
    Route::get('my-profile', 'UserController@myProfile')->name('my.profile');
    Route::post('my-profile-update', 'UserController@updateMyProfile')->name('my.profile.update');
//    Route::get('users-profile/{user}', 'UserController@usersDatatable')->name('users.profile');
});

